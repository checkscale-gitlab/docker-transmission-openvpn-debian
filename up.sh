#!/bin/bash -ex

# TODO force transmission to only use the VPN
# iptables -A OUTPUT -m owner --uid-owner debian-transmission -o lo -j ACCEPT
# iptables -A OUTPUT -m owner --uid-owner debian-transmission \! -o tun0 -j REJECT

{ echo -e "GET / HTTP/1.0\r\n\r" >&3; cat <&3 ; } 3<> /dev/tcp/checkip.dyndns.org/80

cd ~debian-transmission/
sudo --user debian-transmission \
     /usr/bin/transmission-daemon \
    --encryption-required \
    --foreground \
    --log-error \
    &
