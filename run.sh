#!/bin/sh -ex

cd $(dirname $0)

docker run \
       --restart always \
       --cap-add=NET_ADMIN \
       --publish 9091:9091 \
       --sysctl net.ipv6.conf.all.disable_ipv6=0 \
       --volume /var/lib/transmission-daemon:/var/lib/transmission-daemon \
       --volume `pwd`/etc/openvpn/client:/etc/openvpn/client \
       --env VPNCONF=TunnelBearAustria \
       $(basename $(pwd)) \
       > /tmp/$(basename $(pwd)).log 2>&1 \
       &


#       --interactive --tty --rm \
